var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var flakeSize = 1
var steps = 12
var rotation = 0
var scaleRatio = 5

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)


  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  push()
  rotate(rotation * Math.PI)
  scale(scaleRatio)
  translate(-windowWidth * 0.5, -windowHeight * 0.5)
  for (var i = 0; i < steps; i++) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(Math.PI * (1 / 6) * (i % 2))
    drawKochSnowflake(boardSize * flakeSize * pow(sqrt(3) * 0.5 - sqrt(3) * 0.5 * (1 / 3), i), 5, 255, kochSnowflake(5))
    pop()

    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(Math.PI)
    rotate(Math.PI * (1 / 6) * (i % 2))
    drawKochSnowflake(boardSize * flakeSize * pow(sqrt(3) * 0.5 - sqrt(3) * 0.5 * (1 / 3), i), 5, 255, kochSnowflake(5))
    pop()
  }
  pop()
  pop()

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)

  scaleRatio += deltaTime * 0.002
  if (scaleRatio >= 7.785) {
    scaleRatio = 2.595
  }
  rotation += deltaTime * 0.00005
  if (rotation >= 1) {
    rotation = 0
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawKochSnowflake(size, iteration, col, snowflake) {
  var rotation = 0
  var position = [0, 0]
  var vertices = []
  for (var i = 0; i < snowflake.length; i++) {
    var vert = [0, 0]
    if (snowflake[i] === 'F') {
      position[0] += sin(rotation) * size / pow(3, iteration)
      position[1] += cos(rotation) * size / pow(3, iteration)
      vert = position
      vertices.push(vert.slice())
    } else if (snowflake[i] === '+') {
      rotation += Math.PI * (1 / 3)
    } else if (snowflake[i] === '-') {
      rotation += Math.PI * (1 / 3) * (-1)
    }
  }

  fill(col)
  noStroke()
  beginShape()
  for (var i = 0; i < vertices.length; i++) {
    vertex(vertices[i][0], vertices[i][1])
  }
  endShape(CLOSE)
}

function kochSnowflake(iteration) {
  var angle = Math.PI * (1 / 3)
  var axiom = '+F--F--F'
  var rule = 'F+F--F+F'
  var snowflake = axiom.split('')
  for (var i = 0; i < iteration; i++) {
    for (var j = 0; j < snowflake.length; j++) {
      if (snowflake[j] === 'F') {
        snowflake[j] = rule.split('')
      }
    }
    snowflake = snowflake.flat()
  }
  return snowflake
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
